package main

import (
	"fmt"
	"bitbucket.org/athomas_ndt/prototype-pattern/prototype"
)

func main() {
	var h prototype.QuadraticProfessor
	//var h prototype.ConstProfessor
	h.Mutate()
	c := h.Clone()

	fmt.Println("original:", h.Ask(42))
	fmt.Println("clone:", c.Ask(42))

	fmt.Println("mutate original")
	h.Mutate()
	fmt.Println("original:", h.Ask(42))
	fmt.Println("clone:", c.Ask(42))
}