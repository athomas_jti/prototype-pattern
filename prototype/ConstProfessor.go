package prototype

type ConstProfessor struct {
	v int
}

func (h ConstProfessor) Ask(x Question) Question {
	return Question(h.v)
}

func (h ConstProfessor) Clone() Professor {
	return &h
}

func (h *ConstProfessor) Mutate() {
	h.v += 1
}
