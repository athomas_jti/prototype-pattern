package prototype

type Professor interface {
	Ask(Question) Question
	Clone() Professor
	Mutate()
}

type Question int
