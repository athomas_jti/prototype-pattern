package prototype

type QuadraticProfessor struct {
	m int
	b int
}

func (h QuadraticProfessor) Ask(x Question) Question {
	return Question(h.m*int(x) + h.b)
}

func (h QuadraticProfessor) Clone() Professor {
	return &h
}

func (h *QuadraticProfessor) Mutate() {
	h.m += 1
	h.b += 5
}
