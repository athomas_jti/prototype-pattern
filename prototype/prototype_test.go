package prototype

import (
	"testing"
	"testing/quick"
)

func cloneIsSame(x Professor, q Question) bool {
	c := x.Clone()
	y1 := c.Ask(q)
	y2 := x.Ask(q)
	return y1 == y2
}

func cloneIsIndependent(x Professor, q Question) bool {
	c := x.Clone()
	y1 := x.Ask(q)
	x.Mutate()
	y2 := c.Ask(q)
	return y1 == y2
}

func originalIsIndependent(x Professor, q Question) bool {
	c := x.Clone()
	y1 := x.Ask(q)
	c.Mutate()
	y2 := x.Ask(q)
	return y1 == y2
}

func TestQuadraticProfessor(t *testing.T) {
	f := func(m, b int, q Question) bool {
		ok := true
		ok = ok && cloneIsSame(&QuadraticProfessor{m, b}, q)
		ok = ok && cloneIsIndependent(&QuadraticProfessor{m, b}, q)
		ok = ok && originalIsIndependent(&QuadraticProfessor{m, b}, q)
		return ok
	}
	if err := quick.Check(f, nil); err != nil {
		t.Error(err)
	}
}

func TestConstProfessor(t *testing.T) {
	f := func(v int, q Question) bool {
		ok := true
		ok = ok && cloneIsSame(&ConstProfessor{v}, q)
		ok = ok && cloneIsIndependent(&ConstProfessor{v}, q)
		ok = ok && originalIsIndependent(&ConstProfessor{v}, q)
		return ok
	}
	if err := quick.Check(f, nil); err != nil {
		t.Error(err)
	}
}
