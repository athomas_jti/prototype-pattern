### What is this repository for? ###

* This is a quick demo of the Prototype Pattern implemented in golang

### How do I get set up? ###

* [Download go](https://golang.org/dl/)
* [Set your gopath](https://golang.org/doc/code.html#Organization)
* "go fetch bitbucket.org/athomas_ndt/prototype-pattern"

### Further info ###

The following were useful for problems I had when writing this:

* [Interface in golang](https://golang.org/doc/effective_go.html#interface_methods)
* [polymorphic variables](http://stackoverflow.com/questions/13511203/why-cant-i-assign-a-struct-to-an-interface)
* [unit tests](http://www.golang-book.com/12/index.htm)